import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactUsModule } from './components/contact-us/contact-us.module';
import { GetInTouchModule } from './components/get-in-touch/get-in-touch.module';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, ContactUsModule, GetInTouchModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
