import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactUsComponent } from './contact-us.component';
import { GetInTouchModule } from '../get-in-touch/get-in-touch.module';

@NgModule({
  declarations: [ContactUsComponent],
  imports: [CommonModule, GetInTouchModule],
})
export class ContactUsModule {}
